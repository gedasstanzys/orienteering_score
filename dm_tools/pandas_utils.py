

def widen_pd_repr():
    import pandas as pd
    pd.set_option('display.max_rows', 100)
    pd.set_option('display.max_columns', 100)
    pd.set_option('display.width', 220)