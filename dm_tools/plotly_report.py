import uuid
import logging
import os

from plotly import io as pio, offline as py

from mar_research.utils.plotly import get_fname_from_str


logger = logging.getLogger(__name__)


def get_report_blocks(figure_lst, report_dir, block_lst=None):
    if block_lst is None:
        block_lst = []
    for fig in figure_lst:
        title = fig.__rpt_title if hasattr(fig, '__rpt_title') else str(uuid.uuid4())
        html_block = fig_report_block(fig, report_dir, title)
        block_lst.append(html_block)
    return block_lst


def compose_report_document(block_lst, file_path=None, report_name=''):
    rpt_html = \
    """<!DOCTYPE html>
    <html>
     <head>
        <meta charset="utf8"/>
        <style>
            @media print {{
                #footer {{
                    position: fixed;
                    bottom: 0;
                }}
            }}
        </style>
     </head>
     <body>
     {title}
     <br>
     <hr>
     {report_blocks}
     </body>
    </html>
    """.format(title=report_name, report_blocks='\n'.join(block_lst))
    if file_path is not None:
        with open(file_path, "w") as f:
            f.write(rpt_html)
    else:
        return rpt_html


def fig_report_block(fig, report_dir, title='', inline_interactive=False):
    if not os.path.exists(os.path.join(report_dir, 'figs')):
        os.makedirs(os.path.join(report_dir, 'figs'))
    fpath = os.path.join('figs', get_fname_from_str(title))
    # static
    static_fname = fpath + '.png'
    pio.write_image(fig, os.path.join(report_dir, static_fname), width=1920, height=1080, scale=2)
    # interactive
    interactive_fname = fpath + '.html'
    py.plot(fig, filename=os.path.join(report_dir, interactive_fname), auto_open=False)

    static_url, interactive_url = (os.path.join('.', p) for p in [static_fname, interactive_fname])

    if inline_interactive:
        # use just interactive
        report_block = (
            '<iframe style="border: none;" src="{interactive_url}" width="100%" height="600px"></iframe>'
            '<br>' +
            '<hr>'
        )
    else:
        report_block = (
            '{title}' +
            '<br>' + # Line break
            '<a href="{interactive_url}" target="_blank">' +  # Open the interactive graph when you click on the image
                '<img src="{static_url}" style="width:100%">' +
            '</a>' +
            '<hr>')  # horizontal line
    return report_block.format(static_url=static_url, interactive_url=interactive_url, title=title)


def report_link_block(name, url):
    return '\n<br><a href="{url}" target="_blank">{name}</a><br>\n'.format(url=url, name=name)


def report_from_figures(figure_lst, root_report_dir, report_name):
    report_dir_path = os.path.join(root_report_dir, get_fname_from_str(report_name))
    block_lst = get_report_blocks(figure_lst, report_dir_path)

    report_fpath = os.path.join(report_dir_path, 'index.html')
    logger.info('Writing report {} to {}'.format(report_name, report_fpath))
    compose_report_document(block_lst, report_fpath, report_name)
    return report_fpath


def compose_root_report(root_report_dir, subreports_dict, report_name):
    """
    Compose root report containing links to subreports
    """
    block_lst = []
    for name, path in subreports_dict.items():
        relative_subreport_url = os.path.join('.', path.replace(root_report_dir+os.sep, ''))
        block_lst.append(report_link_block(name, relative_subreport_url))
    report_fpath = os.path.join(root_report_dir, 'index.html')
    compose_report_document(block_lst, report_fpath, report_name)