import hashlib
import os
import sys
import logging
import functools

import pandas as pd


logger = logging.getLogger(__name__)
MAIN_FOLDER = '.mar_research_data' + os.path.sep
CACHE_PATH = '~'


def get_cache_path():
    global CACHE_PATH
    return os.path.join(os.path.expanduser(CACHE_PATH), MAIN_FOLDER)


def cache_file_path(node_name, cache_key=None, ext='hdf'):
    if cache_key is None:
        cache_key = 'any'
    folder = get_cache_path()
    # to avoid hitting max file limit, also to structure cache into logical groups
    # (assuming key has top grouping level last)
    cache_key_split = cache_key.split('__')[::-1]
    file_path_parts = [folder] + [node_name] + cache_key_split + ['{}.{}'.format(node_name, ext)]
    return os.path.join(*file_path_parts)


def save_dict_cache(dict_of_df, fpath, mode='w'):
    keys = sorted(dict_of_df.keys())
    key_paths = []
    for key in keys:
        key_path = fpath[:-4] + key.date().isoformat() + fpath[-4:]
        dict_of_df[key].to_hdf(key_path, 'na', mode=mode, complevel=1)
        key_paths.append(key_path.split(MAIN_FOLDER)[1])
    info_df = pd.DataFrame({'keys': keys, 'key_paths': key_paths})
    info_df.to_hdf(fpath, 'info', mode=mode, complevel=1)


def read_dict_cache(fpath):
    try:
        df = pd.read_hdf(fpath, 'info')
        res_dict = {}
        for i, row in df.iterrows():
            res_dict[row['keys']] = pd.read_hdf(fpath.split(MAIN_FOLDER)[0] + MAIN_FOLDER + row['key_paths'].split(MAIN_FOLDER)[1])
        return res_dict
    except (IOError, KeyError) as ex:
        return None


def move_file_to_backup(file_path):
    if os.path.isfile(file_path):
        bk_fpath = '{}.{}.bk'.format(file_path, pd.Timestamp.now().strftime('%Y%m%d_%H%M%S'))
        logger.info('leaving backup in {}'.format(bk_fpath))
        os.rename(file_path, bk_fpath)


def save_cache(df, node_name, cache_key=None, mode='w'):
    fpath = cache_file_path(node_name, cache_key)
    os.makedirs(os.path.split(fpath)[0], exist_ok=True)
    move_file_to_backup(fpath)
    logging.info("saving to cache: {}".format(fpath))
    if isinstance(df, pd.DataFrame) or isinstance(df, pd.Series):
        df.to_hdf(fpath, 'na', mode=mode, complevel=1)
    elif isinstance(df, dict):
        save_dict_cache(df, fpath, mode='w')
    else:
        logger.error('underlying function returned type [{}] which is not suppoted by cached_node,'
                     'skipping cache write:'.format(type(df)))
    return fpath


def read_cache(node_name, cache_key=None):
    fpath = cache_file_path(node_name, cache_key)
    logger.info("trying to load from cache: {}".format(fpath))
    res = read_dict_cache(fpath)
    if res is None:
        res = pd.read_hdf(fpath)
    return res


def _hash(v):
    """Hash that handles some unhash'able types by converting:
            [v,]      -> sorted(_hash(v), ...)
            {k:v,}    -> sorted((_hash(k), _hash(v)), ...)
            None      -> guid string
    """
    type_v = type(v)
    if type_v in [list, tuple, set]:
        v = [_hash(value) for value in v]
    elif type_v == dict:
        v = [(_hash(key), _hash(value)) for (key, value) in v.items()]
    elif v is None:
        v = '8521e939-299b-4a84-a4dd-39d75eacbb14'

    if type_v in [set, dict]:
        v = sorted(v)

    try:
        return repr(v)  # TODO: probably we can be smarter than that
    except:
        raise ValueError('type {} is not supported by gen_key "hash". Repr of value: {}'.format(type(v), v))


def generate_unique_key(args, kwargs):
    """
    Generates a somewhat unique key based on the hashed values of all of the args and kwargs.
    Also see _hash.
    """
    hashed_args = [str(_hash(arg)) for arg in args]
    hashed_kwargs = ['{}:{}'.format(_hash(k), _hash(v)) for k, v in kwargs.items()]
    args_str = ','.join(hashed_args + hashed_kwargs)
    return hashlib.md5(args_str.encode('utf-8')).hexdigest(), args_str


def cached_node(func=None, gen_key=None):
    """
    Minimalistic persistent cache layer for DAG until we have something more solid

    :param func:
    :param gen_key: None | 'hash' - note that gen_key='hash' has limited use and by no means it should be used in
           production as hash code can match between different (args, kwargs) values
    :return:
    """
    if func is None:
        return functools.partial(cached_node, gen_key=gen_key)

    @functools.wraps(func)
    def func_wrapper(*args, **kwargs):
        fn = func.__name__
        node_name = fn[:-3] if fn.endswith('_nd') else fn
        if gen_key == 'hash':
            nd_cache_key, args_str = generate_unique_key(args, kwargs)
        else:
            args_str = ''
            nd_cache_key = kwargs.get('nd_cache_key', 'any')
        logger.debug('cached: node_name={}, nd_cache_key={}, args_str={}'.format(node_name, nd_cache_key, args_str))
        try:
            res = read_cache(node_name, nd_cache_key)
            logger.info('cache loaded')
            return res
        except Exception as ex:  # windows gives "FileNotFoundError: File {} does not exist"
                                 # file_io: "IOError: File xxx does not exist"
            if not 'does not exist' in str(ex):
                raise
        logger.info('no cache, running underlying')
        res = func(*args, **kwargs)
        save_cache(res, node_name, nd_cache_key)
        return res
    return func_wrapper