import string
import math
import os
import uuid
import tempfile
import logging

import pandas as pd

from plotly.subplots import make_subplots
import plotly.offline as py
import plotly.graph_objs as go
import plotly.express as px


logger = logging.getLogger(__name__)


def _fig_to_file(dir_name, fig, fname=None, layout_kwargs={}, to_file=True, add_unique_suffix=True):
    title = layout_kwargs.get('title', '')
    fig.__rpt_title = title
    if to_file:
        if fname is None:
            fname = _get_plot_fname(get_fname_from_str(title), dir_name, add_unique_suffix)
        else:
            fname = _get_plot_fname(fname, dir_name, add_unique_suffix)
        logger.info("Writing figure to: {}".format(fname))
        dir_name = os.path.split(fname)[0]
        if not os.path.isdir(dir_name):
            os.makedirs(dir_name)
        py.plot(fig, filename=fname)


def dfhist(data_series, fname=None, dir_name=None, nbins=100, cumulative=False, to_file=True, **layout_kwargs):
    """
    :param layout_kwargs: will be passed to plotly.graph_objs.Layout
        some commonly used ones:
            title - adds title on the top of the plot
    """
    fig = go.Figure(data=[go.Histogram(x=data_series.values,
                                       histnorm='probability',
                                       cumulative=dict(enabled=cumulative),
                                       nbinsx=nbins
                                       ),
                          ],
                    layout=go.Layout(**layout_kwargs)
                    )
    _fig_to_file(dir_name, fig, fname, layout_kwargs, to_file)
    return fig


def dfheatmap(df, fname=None, dir_name=None, to_file=True, zmin=None, zmax=None, **layout_kwargs):
    """
    :param layout_kwargs: will be passed to plotly.graph_objs.Layout
        some commonly used ones:
            title - adds title on the top of the plot
    """
    def _col_label(index):
        col_lst = index.tolist()  # note will contain multi-columns as tuples
        if col_lst and type(col_lst[0]) == tuple:
            col_lst = ['.'.join(map(str, t)) for t in col_lst]
        return col_lst
    x_labels = _col_label(df.columns)
    y_labels = _col_label(df.index)
    heatmap_kwargs = {}
    if zmin is not None:
        heatmap_kwargs['zmin'] = zmin
    if zmax is not None:
        heatmap_kwargs['zmax'] = zmax
    fig = go.Figure(data=[
                        go.Heatmap(
                            z=df.values.tolist(), colorscale='Viridis',
                            x=x_labels,
                            y=y_labels,
                            **heatmap_kwargs
                        )],
                    layout=go.Layout(**layout_kwargs)
    )
    _fig_to_file(dir_name, fig, fname, layout_kwargs, to_file)
    return fig


def dfboxplot(df, fname=None, dir_name=None, to_file=True, **layout_kwargs):
    """
    :param df: each column will be represented as boxplot
    :return:
    """
    df = df.unstack()
    df = df.reset_index()
    df.columns = ['measure', 'index', 'value']
    fig = px.box(df, x='measure', y='value')
    _fig_to_file(dir_name, fig, fname, layout_kwargs, to_file)
    return fig


def dfviolin(df, fname=None, dir_name=None, to_file=True, box=False, **layout_kwargs):
    """
    :param df: each column will be represented as boxplot
    :return:
    """
    df = df.unstack()
    df = df.reset_index()
    df.columns = ['measure', 'index', 'value']
    fig = px.violin(df, x='measure', y='value', box=box)
    _fig_to_file(dir_name, fig, fname, layout_kwargs, to_file)
    return fig


def subplots(figs, fname=None, dir_name=None, to_file=True, ncols=None, **layout_kwargs):
    '''
    Adds figs as subplots into a roughly square grid.
    :param figs:
    :param fname:
    :param dir_name:
    :param to_file:
    :param layout_kwargs:
    :return:
    '''
    nfigs = len(figs)
    if ncols is None:
        ncols = math.floor(math.sqrt(nfigs))
    nrows = int(math.ceil(nfigs / float(ncols)))

    subplot_titles_lst = [fig.layout.title.text for fig in figs]
    if 'title' not in layout_kwargs:
        layout_kwargs['title'] = '__'.join(subplot_titles_lst)

    resfig = make_subplots(rows=nrows, cols=ncols, subplot_titles=subplot_titles_lst)

    for i, fig in enumerate(figs):
        for trace in fig.data:
            resfig.append_trace(trace, i // ncols + 1, (i % ncols) + 1)

    _fig_to_file(dir_name, resfig, fname, layout_kwargs, to_file)
    return resfig


def dfplot(data, data_yaxis2=None, marker_sizes=None, marker_size_default=3,
           fname=None, dir_name=None, to_file=True, **layout_kwargs):
    """
    Convenience method for pandas object plotting in plotly

    :param data: Series, DataFrame or list of DataFrames:
        index/data to be plotted on scatter X/Y
    :param data_yaxis2: all as per data, but right Y axis to be used
    :param marker_sizes: all as per data, but controls the size of the marker, columns / index
        should match data.columns; values - int / float - uses same scale as marker_size_default
    :param marker_size_default: int or float - marker size to be used for is
    :param layout_kwargs: will be passed to plotly.graph_objs.Layout
        some commonly used ones:
            title - adds title on the top of the plot
    :param to_file: saves plot to .html file and opens it if true
    """

    def _merge_data_into_single_df(data):
        if isinstance(data, pd.Series):
            # convert Series to DF
            df = pd.DataFrame(data)
        elif not isinstance(data, pd.DataFrame):
            # in case list of DFs is give we will concat them first
            df = pd.concat(data, axis=1)
        else:
            df = data.copy()

        # if non unique - make column names unique by adding idx suffix
        if len(df.columns.unique()) != len(df.columns):
            col_lst = ['{}_{}'.format(df.columns[i], i) for i in range(len(df.columns))]
            df.columns = col_lst
        return df

    def _compose_plt_data(data_df, marker_sizes, plt_data, yaxis=None):
        for i in range(len(data_df.columns)):
            col = data_df.columns[i]
            if marker_sizes is not None and col in marker_sizes.columns:
                marker_size_arg = marker_sizes.loc[:, col].fillna(marker_size_default).tolist()
            else:
                marker_size_arg = marker_size_default
            kwargs = dict(marker=dict(size=marker_size_arg))
            s = go.Scatter(x=data_df.index, y=data_df.iloc[:, i].values, name=str(col), yaxis=yaxis,
                           mode='lines+markers', **kwargs)
            plt_data.append(s)

    if marker_sizes is not None:
        marker_sizes = _merge_data_into_single_df(marker_sizes)
    plt_data = []
    _compose_plt_data(_merge_data_into_single_df(data), marker_sizes, plt_data)
    if data_yaxis2 is not None:
        _compose_plt_data(_merge_data_into_single_df(data_yaxis2), marker_sizes, plt_data, yaxis='y2')
        layout_kwargs['yaxis2'] = dict(overlaying='y', side='right')

    layout = go.Layout(**layout_kwargs)
    fig = go.Figure(data=plt_data, layout=layout)
    _fig_to_file(dir_name, fig, fname, layout_kwargs, to_file)
    return fig


def _get_plot_fname(title, dir_name, add_unique_suffix=True):
    dir_name = dir_name or tempfile.gettempdir()
    fname = title + str(uuid.uuid4()) if add_unique_suffix else title
    fname = os.path.join(dir_name, fname + '.html')
    return fname


def get_fname_from_str(s):
    puncts = string.punctuation + ' '
    trans_tbl = str.maketrans(puncts, len(puncts) * '_')
    return s.translate(trans_tbl)