import logging
from contextlib import contextmanager

import webbrowser
from importlib import import_module

from distributed import LocalCluster, Client, WorkerPlugin
import psutil

logger = logging.getLogger(__name__)


@contextmanager
def local_temp_dask_cluster(threads_per_worker=2, num_workers=0, open_in_browser=False,
                            worker_kwargs={
                                # 'memory_target_fraction': 0.6,
                                # 'memory_spill_fraction': 0.7,
                                # 'memory_pause_fraction': 0.8,
                                # 'memory_terminate_fraction': 0.95,
                                # 'memory_limit': '1G',
                                'memory_limit': 0
                            },
                            worker_setup_func_or_str_ref=None
                            ):
    """
    Encapsulates common use-case of local cluster creation
    :param threads_per_worker:
    :param num_workers:
    :param open_in_browser:
    :param worker_kwargs:
    :param worker_setup_func_or_str_ref:
            if type(function) - will be passed to client.register_worker_callbacks
            if str - after split at rightmost dot leftside is interpreted as module and rightside as function to be
                passed to client.register_worker_callbacks
    :return:
    """
    if num_workers == 0:
        num_workers = psutil.cpu_count(logical=False)

    cl_constructor = lambda: LocalCluster(threads_per_worker=threads_per_worker,
                           n_workers=int(num_workers),
                           **worker_kwargs)

    worker_setup_func = worker_setup_func_or_str_ref
    if worker_setup_func_or_str_ref is not None and isinstance(worker_setup_func_or_str_ref, str):
        worker_setup_func = func_by_str_ref(worker_setup_func_or_str_ref)

    with cl_constructor() as cluster, Client(cluster) as client:
        if worker_setup_func_or_str_ref is not None:
            client.register_worker_callbacks(worker_setup_func)

        if open_in_browser:
            si = client.scheduler_info()
            webbrowser.open_new_tab('http://localhost:{}'.format(si['services']['dashboard']))

        yield client


def func_by_str_ref(func_path_str):
    dot_idx = func_path_str.rfind('.')
    module_str = func_path_str[:dot_idx]
    func_str = func_path_str[dot_idx + 1:]
    module = import_module(module_str)
    func = getattr(module, func_str)
    return func
