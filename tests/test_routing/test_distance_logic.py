import webbrowser
from routing.utils import str_to_df
from routing.distance_logic import distance_2points, distance_2ltrs, distance_word, LETTER, CLUSTER_PAIR, LINK_ID, SEQ_NO


ltr_df = str_to_df("""
    letter       lon        lat  cluster
    A       0.112335  51.453198       C2
    H       0.119369  51.457074       C1
    X       0.120871  51.457428       C1
    :       0.115642  51.458068       C1
    .       0.115894  51.457725       C1

""").set_index(LETTER)


clinks_df = str_to_df("""
    cluster_pair link_id seq_no       lon        lat
    C1-C2        2       1       0.122860  51.452922
    C1-C2        2       2       0.122388  51.451531
    C1-C2        1       1       0.113813  51.455379

""").set_index([CLUSTER_PAIR, LINK_ID, SEQ_NO])


class Test_distance_2points:
    def test_basic(self):
        lon1, lat1, lon2, lat2 = 0.116986, 51.452581, 0.121528, 51.456861
        dist_mtr = distance_2points(lat1, lon1, lat2, lon2)
        assert round(dist_mtr, 4) == 570.5592


class Test_distance_2ltrs:

    def test_with_transit(self):
        dist = distance_2ltrs('A', 'H', ltr_df, clinks_df)
        assert round(dist, 4) == 691.8735

    def test_wo_transit(self):
        dist = distance_2ltrs('H', 'X', ltr_df, clinks_df)
        assert round(dist, 4) == 111.2624


class Test_distance_word:

    def test_core(self):
        dist = distance_word('AHX', ltr_df, clinks_df)
        assert round(dist, 4) == 803.1358


def visual_coords_test():
    lon1, lat1, lon2, lat2 = 0.116986, 51.452581, 0.121528, 51.456861
    url = """https://www.google.com/maps/search/?api=1&query={lat},{lon}"""
    webbrowser.open(url.format(lon=lon1, lat=lat1))
    webbrowser.open(url.format(lon=lon2, lat=lat2))
    distance_2points(lat1, lon1, lat2, lon2)
