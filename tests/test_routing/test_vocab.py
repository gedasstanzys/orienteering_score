import pandas as pd

from routing.vocab import is_acceptable_word
from routing.vocab import word_to_points, WORD_LETTER_SCORES


def test_word_to_points():
    assert word_to_points('WIX') == 13
    df = pd.DataFrame.from_dict(WORD_LETTER_SCORES, orient='index').reset_index()
    df.columns = 'letter', 'points'
    df.groupby('points').count()


class Test_is_acceptable_word:

    def test_double(self):
        assert is_acceptable_word('SLEEP') is False

    def test_two_words(self):
        assert is_acceptable_word('CHURCH HAT') is False

    def test_church(self):
        assert is_acceptable_word('CHURCH')

    def test_special_word_above_max(self):
        assert is_acceptable_word('CHRISTMAS')

    def test_9ltrs(self):
        assert is_acceptable_word('CELEBRATE') is False
