import pytest
from pandas._testing import assert_frame_equal

from routing.distance_logic import LETTER, CLUSTER_PAIR, LINK_ID, SEQ_NO
from routing.routing_logic import route_search
from routing.utils import str_to_df

ltr_df = str_to_df("""
    letter       lon        lat  cluster
    A       0.112335  51.453198       C2
    H       0.119369  51.457074       C1
    X       0.120871  51.457428       C1
    :       0.115642  51.458068       C1
    .       0.115894  51.457725       C1

""").set_index(LETTER)


clinks_df = str_to_df("""
    cluster_pair link_id seq_no       lon        lat
    C1-C2        2       1       0.122860  51.452922
    C1-C2        2       2       0.122388  51.451531
    C1-C2        1       1       0.113813  51.455379

""").set_index([CLUSTER_PAIR, LINK_ID, SEQ_NO])


w_scores_df = str_to_df("""
    word   points    distance  distance_per_point
    AHA         3          99                  33
    HAXA        5         100                  25

""").set_index('word')


class Test_route_search:
    def test_basic(self):
        total_distance_limit = 3000
        n_next_cheapest_to_try = 4

        routes_df = route_search(ltr_df.copy(), clinks_df.copy(), w_scores_df.copy(),
                                 total_distance_limit, n_next_cheapest_to_try)
        expected_df = str_to_df("""
              word  cumulative_distance
                 :             0.000000
              HAXA           380.885082
                 .           942.196476
                 :             0.000000
               AHA           687.001762
              HAXA          1478.875245
                 .          2040.186639
        """)
        assert_frame_equal(routes_df[['word', 'cumulative_distance']], expected_df)
