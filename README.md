# What is this?
Quick and dirty route optimiser algo for DFOK Christmas event in Danson park, although other coordinates can be given through google My Maps .kml export.

# How to run this?
Assuming you have some recent python 3 environment, in the root folder of the cloned repository run:
```bash
pip install -e .
python routing/main.py 
```
