import uuid

import numpy as np

from routing.distance_logic import FINISH_NAME
from routing.utils import print_progress_bar


def _join_distance_to_letter(ltr_dist_s, w_stats_df, letter_distance_to, wscores_ltr_col_name='first_ltr'):
    ltr_dist_s_slc = ltr_dist_s.loc[letter_distance_to]
    ltr_dist_s_slc.name = wscores_ltr_col_name+'_distance'
    w_stats_df = w_stats_df.join(ltr_dist_s_slc, on=wscores_ltr_col_name)
    return w_stats_df


def _dfs(route_steps, w_scores_df, ltr_dist_s, total_distance_limit, n_next_cheapest_to_try, _p_info=None):
    """Specialised depth first search"""
    cdef int _p_depth, _p_d2_start, _p_total
    _p_depth, _p_d2_start, _p_total = (0, 0, n_next_cheapest_to_try ** 2) if _p_info is None else _p_info

    word, word_distance, cumulative_distance = route_steps[-1]
    word_last_ltr = word[-1]

    w_stats_df = w_scores_df.loc[w_scores_df['first_ltr'] != word_last_ltr]
    # add distance to first letter (since last word last letter)
    w_stats_df = _join_distance_to_letter(ltr_dist_s, w_stats_df, word_last_ltr, 'first_ltr')
    # add a fraction of distance to finish
    if cumulative_distance / total_distance_limit > .5:
        w_stats_df = _join_distance_to_letter(ltr_dist_s, w_stats_df, FINISH_NAME, 'last_ltr')
        w_stats_df['last_ltr_distance'] *= dtf_weight((cumulative_distance + w_stats_df['distance']) / total_distance_limit)
    else:
        w_stats_df['last_ltr_distance'] = 0
    # adjust dpp to reflect distances to first and last letters
    w_stats_df['distance_per_point'] += (w_stats_df['first_ltr_distance'] + w_stats_df['last_ltr_distance']) / w_stats_df['points']

    w_stats_df.sort_values('distance_per_point', inplace=True)
    # use topX ranked words
    n = 0
    routes = []
    for w, row in w_stats_df.iterrows():
        step_distance = row['distance'] + row['first_ltr_distance']
        distance_to_finish = ltr_dist_s.loc[(w[-1], FINISH_NAME)]
        if (cumulative_distance + step_distance + distance_to_finish) <= total_distance_limit:
            n += 1
            if _p_depth == 1:
                print_progress_bar(_p_d2_start+n, _p_total, prefix='Progress:', suffix='Complete', length=50)
            t_route_steps = route_steps.copy()
            t_route_steps.append((w, step_distance, cumulative_distance + step_distance))
            t_w_scores_df = w_scores_df.drop(w)
            rts = _dfs(t_route_steps, t_w_scores_df, ltr_dist_s, total_distance_limit, n_next_cheapest_to_try, (_p_depth+1, (n-1)*n_next_cheapest_to_try, _p_total))
            routes.extend(rts)

        if n >= n_next_cheapest_to_try:
            break
    if len(routes) == 0:
        distance_to_finish = ltr_dist_s.loc[(word_last_ltr, FINISH_NAME)]
        # route in route_steps had no improvements found in the loop above, - we terminate this route here
        route_id = uuid.uuid4().hex
        # add distance to finish
        route_steps.append((FINISH_NAME, distance_to_finish, cumulative_distance + distance_to_finish))
        return [(route_id, word, cumulative_distance) for word, word_distance, cumulative_distance in route_steps]
    else:
        # return extended routes as collected from nested _dfs calls
        return routes


def dtf_weight(x):
    """
    Returns distance-to-finish multiplier to scale distance to finish to be used while evaluating a word based on
    fraction of total distance limit provided in x
    :param x: 0..1 fraction of distance to finish left
    :return: 0..1
    """
    return 1/(1+np.exp(-x*36+30))