import logging
import os
import time

import routing as rt
from routing.mkl_reader import load_and_parse_placemarks, prepare_placemarks
from routing.routing_logic import route_search
from routing.utils import df_print_width
from routing.vocab import load_w_scores_df, build_w_scores_df


logger = logging.getLogger()
logging.basicConfig(level=logging.INFO)


def main():
    df_print_width()

    base_path = os.path.dirname(rt.__file__)
    danson_2020 = os.path.join(base_path, 'danson_christmas_event.kml')
    footscray_2021 = os.path.join(base_path, 'footscray_christmas_event_2021.kml')

    coords_df = load_and_parse_placemarks(footscray_2021)
    ltr_df, clinks_df = prepare_placemarks(coords_df)

    scored_words_csv_path = os.path.join(base_path, 'w_scores_df.csv')
    build_w_scores_df(ltr_df, clinks_df, scored_words_csv_path)
    w_scores_df = load_w_scores_df(scored_words_csv_path).sort_values('distance_per_point').head(500)
    print('Vocabulary size: {}'.format(len(w_scores_df)))

    total_distance_limit = 14000
    n_next_cheapest_to_try = 2

    start = time.time()
    routes_df = route_search(ltr_df, clinks_df, w_scores_df, total_distance_limit, n_next_cheapest_to_try)
    end = time.time()
    logger.info((end-start))

    # calc route stats
    route_stats_df = calc_route_stats(routes_df, w_scores_df, total_distance_limit)
    route_stats_df.to_clipboard()


def calc_route_stats(routes_df, w_scores_df, total_distance_limit):
    route_stats_df = routes_df.join(w_scores_df['points'], on='word')
    total_points = route_stats_df.groupby('route_id')['points'].sum()
    total_points.name = 'total_points'
    total_distance = route_stats_df.groupby('route_id')['cumulative_distance'].max()
    total_distance.name = 'total_distance'
    route_stats_df = route_stats_df.join(total_points, on='route_id').join(total_distance, on='route_id')
    route_stats_df['distance_to_finish'] = total_distance_limit - route_stats_df['total_distance']
    route_stats_df = route_stats_df.sort_values(['total_points', 'distance_to_finish', 'route_id'], ascending=False)
    return route_stats_df


if __name__ == "__main__":
    main()