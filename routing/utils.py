from io import StringIO
import pandas as pd


def str_to_df(str, rstrip_str='_', set_no_name_index=False, **kwargs):
    str = str.lstrip('\n')
    df = pd.read_fwf(StringIO(str), skipfooter=1, **kwargs)
    if rstrip_str is not None:
        df.columns = [s.rstrip(rstrip_str) for s in df.columns]
    if set_no_name_index:
        df = df.set_index('index', drop=True)
        df.index.name = None
    return df


def df_print_width():
    # pd.set_option('display.height', 1000)
    # pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 100)
    pd.set_option('display.width', 1000)


def print_progress_bar (iteration, total, prefix = '', suffix = '', decimals = 1, length = 100, fill = '█', printEnd = "\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        printEnd    - Optional  : end character (e.g. "\r", "\r\n") (Str)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print(f'\r{prefix} |{bar}| {percent}% {suffix}', end = printEnd)
    # Print New Line on Complete
    if iteration == total:
        print()
