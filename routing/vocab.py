import os

import nltk
import pandas as pd
from nltk.corpus import words

from routing.distance_logic import distance_word
from wordfreq import word_frequency

MIN_WORD_FREQ_SCORE_TO_INCLUDE = 0.00000157

WORD_LETTER_SCORES = {
    'A': 1,'B': 3,'C': 3,'D': 2,'E': 1,'F': 4,'G': 2,'H': 4,'I': 1,'J': 8,'K': 5,'L': 1,'M': 3,'N': 1,'O': 1,'P': 3,
    'Q': 10,'R': 1,'S': 1,'T': 1,'U': 1,'V': 4,'W': 4,'X': 8,'Y':4
}


def word_to_points(w):
    points = 0
    for c in w:
        points = points + WORD_LETTER_SCORES[c]
    return points


def is_acceptable_word(w, min_len=3, max_len=8):
    w = w.replace("'", '').upper()
    lenw = len(w)
    for i in range(lenw-1):
        no_test = w[i : i + 2]
        if len(set(no_test)) == 1:
            return False
    return ' ' not in w \
           and (lenw <= max_len ) \
           and lenw >= min_len \
           and 'Z' not in w


def calc_word_scores(clinks_df, ltr_df, words):
    wscores = {}
    for w in words:
        wscores[w] = word_to_points(w), distance_word(w, ltr_df, clinks_df)
    wscores_df = pd.DataFrame.from_dict(wscores, orient='index', columns=['points', 'distance'])
    wscores_df['distance_per_point'] = wscores_df['distance'] / wscores_df['points']
    wscores_df = wscores_df.sort_values('distance_per_point')
    return wscores_df


def load_nltk_words():
    nltk_datap = os.path.split(os.path.dirname(routing.__file__))[0]  # path containing nltk corpora folder
    nltk.data.path.append(nltk_datap)
    vocab = words.words()
    vocab = [w for w in vocab if w[0].upper() != w[0]]  # remove proper nouns (starting with capitals)
    return vocab


def build_w_scores_df(ltr_df, clinks_df, scored_words_csv_path):
    vocab = load_nltk_words()
    word_set = {w.upper() for w in vocab if is_acceptable_word(w)}
    w_scores_df = calc_word_scores(clinks_df, ltr_df, word_set)

    w_scores_df = w_scores_df.reset_index()
    w_scores_df['word_frequency'] = w_scores_df['word'].apply(lambda w: word_frequency(w, 'en'))

    w_scores_df.to_csv(scored_words_csv_path, index=False)
    return w_scores_df


def load_w_scores_df(scored_words_csv_path):
    w_scores_df = pd.read_csv(scored_words_csv_path, index_col=0)
    w_scores_df = w_scores_df.loc[(w_scores_df['word_frequency'] > MIN_WORD_FREQ_SCORE_TO_INCLUDE) & w_scores_df['exclude'].isnull()]
    return w_scores_df