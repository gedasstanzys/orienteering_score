from itertools import combinations

import pandas as pd

from routing.distance_logic import distance_2ltrs, START_NAME
from routing.routing_logic_speedups_py import _dfs


def route_search(ltr_df, clinks_df, w_scores_df, total_distance_limit, n_next_cheapest_to_try):

    ltr_pairs = list(combinations(ltr_df.index, 2))
    ltr_dists = [distance_2ltrs(ltr_pair[0], ltr_pair[1], ltr_df, clinks_df) for ltr_pair in ltr_pairs]
    ltr_dist_s = pd.Series(index=pd.MultiIndex.from_tuples(ltr_pairs), data=ltr_dists, name='letter_distance')
    ltr_dist_s = pd.concat([ltr_dist_s, ltr_dist_s.swaplevel()])

    # word, word_distance, cumulative_distance
    route_steps = [(START_NAME, 0., 0.)]
    w_scores_df = w_scores_df.copy()
    w_scores_df['first_ltr'] = w_scores_df.index.str[0]
    w_scores_df['last_ltr'] = w_scores_df.index.str[-1]

    routes = _dfs(route_steps, w_scores_df, ltr_dist_s, total_distance_limit, n_next_cheapest_to_try)
    routes_df = pd.DataFrame.from_records(routes, columns=['route_id', 'word', 'cumulative_distance'])

    return routes_df
