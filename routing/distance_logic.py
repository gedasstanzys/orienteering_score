import math

import numpy as np

R = 6371e3  # metres
CLUSTER = 'cluster'
LON = 'lon'
LAT = 'lat'

START_NAME = ':'
FINISH_NAME = '.'
CLINK_NAME = 'clink'
LETTER = 'letter'
DEFAULT_CLUSTER = 'C1'
CLUSTER_PAIR, LINK_ID, SEQ_NO = 'cluster_pair', 'link_id', 'seq_no'


def dtr(degr):
    return degr * np.pi / 180


def distance_2points(lat1, lon1, lat2, lon2):
    theta1 = dtr(lat1)  # φ, λ in radians
    theta2 = dtr(lat2)
    delta_theta = (lat2 - lat1) * np.pi / 180  # difference lat2,lat1
    delta_lambda = (lon2 - lon1) * np.pi / 180  # difference lon2, lon1

    a = (np.sin(delta_theta / 2) * np.sin(delta_theta / 2) +
         np.cos(theta1) * np.cos(theta2) *
         np.sin(delta_lambda / 2) * np.sin(delta_lambda / 2))
    c = 2 * math.atan2(np.sqrt(a), np.sqrt(1 - a))

    d = R * c  # in metres
    return d


def distance_2ltrs(l1: str, l2: str, ltr_df, clinks_df):
    # l1, l2 = 'A', 'H'
    l1_clust, l1_lon, l1_lat = ltr_df.loc[l1, [CLUSTER, LON, LAT]]
    l2_clust, l2_lon, l2_lat = ltr_df.loc[l2, [CLUSTER, LON, LAT]]
    clinks_df = clinks_df.sort_index()  # we assume sorted seq_no while iterating transit steps below
    if l1_clust != l2_clust:
        cpair = '-'.join(sorted([l1_clust, l2_clust]))
        transit_df = clinks_df.loc[cpair]
        # find shortest transit
        link_costs = []
        for try_link_id, tdf in transit_df.groupby(LINK_ID):
            dist, cur_lon, cur_lat = 0., l1_lon, l1_lat
            for _, trow in tdf.iterrows():
                dist += distance_2points(cur_lat, cur_lon, trow[LAT], trow[LON])
                cur_lat, cur_lon = trow[LAT], trow[LON]
            # plus cost to l2
            dist += distance_2points(cur_lat, cur_lon, l2_lat, l2_lon)
            link_costs.append((dist, try_link_id))
        dist = sorted(link_costs)[0][0]
    else:
        dist = distance_2points(l1_lat, l1_lon, l2_lat, l2_lon)
    return dist


def distance_word(wrd: str, ltr_df, clinks_df):
    assert len(wrd) >= 2
    dist = 0
    for i in range(len(wrd)-1):
        l1, l2 = wrd[i:i+2]
        dist += distance_2ltrs(l1, l2, ltr_df, clinks_df)
    return dist
