from pykml import parser
import pandas as pd

from routing.distance_logic import LON, LAT, CLINK_NAME, CLUSTER_PAIR, LINK_ID, SEQ_NO, START_NAME, FINISH_NAME, LETTER, \
    CLUSTER, DEFAULT_CLUSTER


def load_and_parse_placemarks(kml_file='/Users/adm/src/orienteering_score/routing/danson_christmas_event.kml'):
    with open(kml_file) as f:
        doc = parser.parse(f)
    root = doc.getroot()

    row_lst = []
    for pm in root.Document.Folder.Placemark:
        lon, lat, z = map(float, pm.Point.coordinates.text.strip('\n ').split(','))
        row_lst.append((pm.name.text, lon, lat, z))

    df = pd.DataFrame(row_lst, columns=['name', LON, LAT, 'z'])
    return df


def prepare_placemarks(coord_df):
    """
    :param coord_df:
    :return: ltr_df, clinks_df
    """
    assert coord_df.name.is_unique
    # cluster links (clinks) are point on the map that need to be passed to transit from one clister to another.
    # For example, if we have clusters C1 and C2, and a passage is short (not worth multiple the effort to define
    # multiple points) mark one point in the center of the passage an label:
    # clink_C1-C2_1
    # lets say the same two clusters have another, longer passage in a different location, and if we want it demarked
    # with 2 points we would use labels:
    # clink_C1-C2_2_1
    # clink_C1-C2_2_2
    clink_mask = coord_df.name.str.startswith(CLINK_NAME)
    assert any(clink_mask)
    # parse cluster links
    clinks_df = coord_df.loc[clink_mask]
    keys_df = clinks_df.name.str.split('_', expand=True, n=3)
    keys_df = keys_df.iloc[:, 1:]  # drop column1=CLINK_NAME
    # each cluster pair can have multiple link points marked with unique link_id's, while seq_no indicates how
    # a number of points to be taken in order for particular link_id to be passed
    keys_df.columns = [CLUSTER_PAIR, LINK_ID, SEQ_NO]
    keys_df[SEQ_NO] = keys_df[SEQ_NO].fillna(1)
    clinks_df = clinks_df.join(keys_df)
    clinks_df = clinks_df.set_index([CLUSTER_PAIR, LINK_ID, SEQ_NO])

    # parse letters
    ltr_df = coord_df.loc[~clink_mask]
    assert START_NAME in ltr_df.name.values and FINISH_NAME in ltr_df.name.values
    keys_df = ltr_df.name.str.split('_', expand=True, n=2)
    keys_df.columns = [LETTER, CLUSTER]
    keys_df[CLUSTER] = keys_df[CLUSTER].fillna(DEFAULT_CLUSTER)
    ltr_df = ltr_df.join(keys_df).set_index(LETTER)
    return ltr_df, clinks_df