from setuptools import setup, Extension
from setuptools import find_packages
try:
    from Cython.Build import cythonize
except ImportError:
    def cythonize(*args, **kwargs):
        from Cython.Build import cythonize
        return cythonize(*args, **kwargs)

with open('README.md') as f:
    readme = f.read()

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    setup_requires=[
        # Setuptools 18.0 properly handles Cython extensions.
        'setuptools>=18.0',
        'cython',
    ],
    # ext_modules=
    # [
    #     cythonize('routing/routing_logic_speedups.pyx', annotate=True),
        # cythonize('routing/distance_logic.pyx'),
    # ],
    name='routing',
    version='0.102',
    description='',
    long_description=readme,
    author='GS',
    author_email='',
    packages=find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3.7',
    ],
    install_requires=requirements,
    entry_points={
        'console_scripts': [
        ],
    },
)
